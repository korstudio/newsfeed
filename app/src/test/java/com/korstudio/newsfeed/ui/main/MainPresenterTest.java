package com.korstudio.newsfeed.ui.main;

import com.korstudio.newsfeed.BuildConfig;
import com.korstudio.newsfeed.data.NewsItem;
import com.korstudio.newsfeed.data.NewsResponse;
import com.korstudio.newsfeed.service.api.NewsService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by korstudio on 6/5/17.
 */
public class MainPresenterTest {
    private MainPresenter mainPresenter;

    @Mock
    MainScreenContract.View view;
    @Mock
    NewsService service;

    @Mock
    Call<NewsResponse> newsServiceCall;
    @Captor
    ArgumentCaptor<Callback<NewsResponse>> newsServiceCaptor;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(service.getLatestNews(BuildConfig.NEWS_API_SOURCE, BuildConfig.NEWS_API_KEY)).thenReturn(newsServiceCall);

        mainPresenter = new MainPresenter(view, service);
    }


    @Test
    public void retrieveLatestNews_SuccessAPICall_ResponseSuccess() throws Exception {
        mainPresenter.retrieveLatestNews();
        verify(newsServiceCall).enqueue(newsServiceCaptor.capture());

        newsServiceCaptor.getValue().onResponse(newsServiceCall, Response.success(new NewsResponse()));
        verify(view).updateNewsList(new ArrayList<NewsItem>());
    }

    @Test
    public void retrieveLatestNews_APICallError_ResponseFail() throws Exception {
        mainPresenter.retrieveLatestNews();
        verify(newsServiceCall).enqueue(newsServiceCaptor.capture());

        ResponseBody responseBody = ResponseBody.create(MediaType.parse(""), "");
        newsServiceCaptor.getValue().onResponse(newsServiceCall, Response.<NewsResponse>error(401, responseBody));
        verify(view).showErrorDialog();
    }

}