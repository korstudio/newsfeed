package com.korstudio.newsfeed.util;

import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * Created by korstudio on 6/5/17.
 */

public class NewsItemBindingUtil {
    @BindingAdapter({"url"})
    public static void setButtonOpenLinkOnClick(final Button button, final String url) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentUtil.openLink(view.getContext(), url);
            }
        });
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImageFromUrl(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url)
                .into(imageView);
    }
}
