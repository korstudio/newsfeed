package com.korstudio.newsfeed.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by korstudio on 6/5/17.
 */

public class IntentUtil {
    public static void openLink(Context context, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(intent);
    }
}
