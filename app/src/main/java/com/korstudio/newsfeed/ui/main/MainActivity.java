package com.korstudio.newsfeed.ui.main;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.korstudio.newsfeed.R;
import com.korstudio.newsfeed.data.NewsItem;
import com.korstudio.newsfeed.service.ServiceFactory;
import com.korstudio.newsfeed.service.api.NewsService;
import com.korstudio.newsfeed.ui.main.adapter.NewsListAdapter;
import com.korstudio.newsfeed.widget.listener.RecyclerViewItemClickListener;
import com.yalantis.phoenix.PullToRefreshView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity implements MainScreenContract.View {

    @BindView(R.id.pull_to_refresh)
    PullToRefreshView pullToRefreshView;
    @BindView(R.id.list_news)
    RecyclerView newsListView;
    NewsListAdapter newsListAdapter;

    private MainPresenter presenter;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

        init();
        loadNewsList();
    }

    private void init() {
        NewsService service = ServiceFactory.create(NewsService.class);
        presenter = new MainPresenter(this, service);

        newsListView.setLayoutManager(new LinearLayoutManager(this));
        newsListAdapter = newsListAdapter == null ? new NewsListAdapter() : newsListAdapter;
        newsListView.setAdapter(newsListAdapter);

        pullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadNewsList();
            }
        });
    }

    private void loadNewsList() {
        presenter.retrieveLatestNews();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void updateNewsList(@NonNull List<NewsItem> newsItemList) {
        newsListAdapter.setItems(newsItemList);
        pullToRefreshView.setRefreshing(false);
    }

    @Override
    public void showErrorDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.error_something_went_wrong)
                .setNeutralButton(R.string.common_dismiss, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }
}
