package com.korstudio.newsfeed.ui.main;

import com.korstudio.newsfeed.BuildConfig;
import com.korstudio.newsfeed.data.NewsResponse;
import com.korstudio.newsfeed.service.api.NewsService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by korstudio on 6/4/17.
 */
public class MainPresenter implements MainScreenContract.Action {

    private final MainScreenContract.View view;
    private final NewsService service;

    public MainPresenter(MainScreenContract.View view, NewsService service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void retrieveLatestNews() {
        service.getLatestNews(BuildConfig.NEWS_API_SOURCE, BuildConfig.NEWS_API_KEY).enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                if(response.isSuccessful()) {
                    NewsResponse newsResponse = response.body();
                    if(newsResponse != null) {
                        view.updateNewsList(newsResponse.articles);
                    }
                }else{
                    view.showErrorDialog();
                }
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
                view.showErrorDialog();
            }
        });
    }
}
