package com.korstudio.newsfeed.ui.main;

import com.korstudio.newsfeed.data.NewsItem;

import java.util.List;

/**
 * Created by korstudio on 6/4/17.
 */
public interface MainScreenContract {
    interface View {
        void updateNewsList(List<NewsItem> newsItemList);
        void showErrorDialog();
    }

    interface Action {
        void retrieveLatestNews();
    }
}
