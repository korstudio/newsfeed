package com.korstudio.newsfeed.ui.main.adapter;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.korstudio.newsfeed.BR;
import com.korstudio.newsfeed.R;
import com.korstudio.newsfeed.common.BindingViewHolder;
import com.korstudio.newsfeed.data.NewsItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by korstudio on 6/4/17.
 */
public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.NewsItemViewHolder> {

    private List<NewsItem> newsItemList = new ArrayList<>();

    public void setItems(List<NewsItem> newsItemList) {
        this.newsItemList.clear();
        this.newsItemList.addAll(newsItemList);

        notifyDataSetChanged();
    }

    @Override
    public NewsListAdapter.NewsItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_news, parent, false);
        return new NewsListAdapter.NewsItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsListAdapter.NewsItemViewHolder holder, int position) {
        NewsItem newsItem = newsItemList.get(position);

        ViewDataBinding viewBinder = holder.getViewBinder();
        viewBinder.setVariable(BR.item, newsItem);
        viewBinder.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return newsItemList.size();
    }

    static class NewsItemViewHolder extends BindingViewHolder {
        @BindView(R.id.coverImage)
        ImageView coverImage;

        NewsItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
