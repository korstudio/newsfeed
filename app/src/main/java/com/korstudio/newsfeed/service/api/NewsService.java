package com.korstudio.newsfeed.service.api;

import com.korstudio.newsfeed.BuildConfig;
import com.korstudio.newsfeed.data.NewsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by korstudio on 6/4/17.
 */
public interface NewsService {
    @GET("articles?sortBy=latest")
    Call<NewsResponse> getLatestNews(@Query("source") String source, @Query("apiKey") String apiKey);

    @GET("articles?sortBy=top")
    Call<NewsResponse> getTopNews(@Query("source") String source, @Query("apiKey") String apiKey);
}
