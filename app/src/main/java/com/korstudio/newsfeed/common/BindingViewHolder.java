package com.korstudio.newsfeed.common;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by korstudio on 6/4/17.
 */
public class BindingViewHolder extends RecyclerView.ViewHolder {
    private final ViewDataBinding viewBinder;

    public BindingViewHolder(View itemView) {
        super(itemView);
        viewBinder = DataBindingUtil.bind(itemView);
    }

    public ViewDataBinding getViewBinder() {
        return viewBinder;
    }
}
