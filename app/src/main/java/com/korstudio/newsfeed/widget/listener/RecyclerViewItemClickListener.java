package com.korstudio.newsfeed.widget.listener;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by korstudio on 6/4/17.
 */
public class RecyclerViewItemClickListener implements RecyclerView.OnItemTouchListener {
    private final Context context;
    private final OnItemClickListener listener;
    private GestureDetector gestureDetector;

    public RecyclerViewItemClickListener(Context context, OnItemClickListener listener) {
        this.context = context;
        this.listener = listener;

        setupGesture();
    }

    private void setupGesture() {
        gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View childView = rv.findChildViewUnder(e.getX(), e.getY());
        if(childView != null && listener != null && gestureDetector.onTouchEvent(e)) {
            listener.onClick(childView, rv.getChildAdapterPosition(childView));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public interface OnItemClickListener {
        void onClick(View view, int position);
    }
}
