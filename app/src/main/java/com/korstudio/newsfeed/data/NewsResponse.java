package com.korstudio.newsfeed.data;

import com.google.gson.annotations.SerializedName;
import com.korstudio.newsfeed.BuildConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by korstudio on 6/4/17.
 */
public class NewsResponse {
    @SerializedName("status")
    public String status = "";
    @SerializedName("source")
    public String source = BuildConfig.NEWS_API_SOURCE;
    @SerializedName("sortBy")
    public String sortBy = "latest";
    @SerializedName("articles")
    public List<NewsItem> articles = new ArrayList<>();
}
