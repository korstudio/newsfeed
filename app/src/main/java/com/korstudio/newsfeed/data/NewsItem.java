package com.korstudio.newsfeed.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by korstudio on 6/4/17.
 */
public class NewsItem {

    /**
     * author : Greg Kumparak
     * title : Apple brings dancing robots and backflipping drones into Swift Playgrounds
     * description : Code rules everything around me. And you. Really: be it the stoplight you stared at this morning, or the train you rode in on, or the lil’ robot vacuum..
     * url : https://techcrunch.com/2017/06/01/dancing-robots-backflipping-drones-apple-wants-to-teach-you-to-program-hardware-with-your-ipad/
     * urlToImage : https://img.vidible.tv/prod/2017-06/01/59305068955a316f1c36db53/593050e06df6792e08e93819_o_U_v1.jpg?w=764&h=400
     * publishedAt : 2017-06-01T15:00:41Z
     */

    @SerializedName("author")
    public String author = "";
    @SerializedName("title")
    public String title = "";
    @SerializedName("description")
    public String description = "";
    @SerializedName("url")
    public String url = "";
    @SerializedName("urlToImage")
    public String urlToImage = "";
    @SerializedName("publishedAt")
    public String publishedAt = "";
}
